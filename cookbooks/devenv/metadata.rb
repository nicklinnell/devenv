name             'devenv'
maintainer       "Nick Linnell"
maintainer_email "nicklinnell@gmail.com"
license          'All rights reserved'
description      'Cookbook to manage your development environment'
version          "0.0.1"

depends 'apt'
depends 'locale'
depends 'git'
depends 'rubies'
depends 'tmux'
depends 'vim'
depends 'homesick'
depends 'mysql'
depends 'database'
