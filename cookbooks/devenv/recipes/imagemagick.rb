#
# Installs imagemagick and dev libraries
#

case node['platform_family']
when 'rhel'
	  package 'ImageMagick'
	  package 'ImageMagick-devel'
when 'debian', 'mac_os_x'
	  package 'imagemagick'
		package 'libmagickcore-dev'
		package 'libmagickwand-dev'
end
