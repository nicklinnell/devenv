include_recipe 'apt'
package 'build-essential'
include_recipe 'git'

link "/home/vagrant/.ssh/known_hosts" do
	to "/home/vagrant/sshkeys/known_hosts"
	user 'vagrant'
	group 'vagrant'
end

link "/home/vagrant/.ssh/config" do
	to "/home/vagrant/sshkeys/config"
	user 'vagrant'
	group 'vagrant'
end

include_recipe 'locale'
include_recipe 'rubies'
include_recipe 'mysql::server'
include_recipe 'mysql::client'
include_recipe 'homesick'
include_recipe 'tmux'
include_recipe 'vim'

homesick_castle 'dotfiles' do
	user 'vagrant'
	source 'git@bitbucket.org:nicklinnell/dotfiles.git'
end

# Powerline
package "python-pip" do
	action :upgrade
end
package "fontconfig" do
	action :upgrade
end

execute "pip install --user git+git://github.com/Lokaltog/powerline" do
	user "vagrant"
	action :run
	environment ({'HOME' => "/home/vagrant", 'USER' => "vagrant"})                                                             
end

ruby_block "update .profile" do
	block do
		file = Chef::Util::FileEdit.new("/home/vagrant/.profile")
		file.insert_line_if_no_match(
			'# Powerline',
			"\n# Powerline\nif [ -d \"$HOME/.local/bin\" ]; then\n    PATH=\"$HOME/.local/bin:$PATH\"\nfi\nif [ -f ~/.local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh ]; then\n source ~/.local/lib/python2.7/site-packages/powerline/bindings/bash/powerline.sh\nfi\nexport TERM=xterm-256color"
		)
		file.write_file
	end
end

# Install vundle in vim
directory "/home/vagrant/.vim/bundle" do
	action :create
	user "vagrant"
	group "vagrant"
end

git "/home/vagrant/.vim/bundle/vundle" do
	repository "https://github.com/gmarik/vundle.git"
	reference "master"
	action :checkout
	user "vagrant"
	group "vagrant"
end

execute "vim +BundleInstall! +qall" do
	user "vagrant"
	action :run
	environment ({'HOME' => "/home/vagrant", 'USER' => "vagrant"})                                                             
end

include_recipe "database::mysql"

mysql_connection_info = {
	:host     => 'localhost',
	:username => 'root',
	:password => ''
}

mysql_database_user 'vagrant' do
	connection mysql_connection_info
	password   ''
	action     :grant
end

mysql_database_user 'vagrant' do
	connection mysql_connection_info
	password   ''
	host 			 '%'
	action     :grant
end

package "exuberant-ctags"
package "ack-grep"
package 'silversearcher-ag'
