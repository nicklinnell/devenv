override['locale']['lang'] = 'en_GB.utf8'
override['locale']['lc_all'] = 'en_GB.utf8'
default['rubies']['list'] = [ 'ruby 2.1.2' ]
default['rubies']['install_bundler'] = true
default['chruby_install']['default_ruby'] = 'ruby-2.1.2'
override['mysql'] = {
	"server_root_password" => "",
	"server_repl_password" => "",
	"server_debian_password" => ""
}
